<?php
    include("includes/head.php");
?>

<section class="about">
    <?php
        include("includes/header.php");
    ?>
    <div class="same_landing about_landing">
        <div class="container">
            <div class="row">
                <p>Bizim Hekayəmiz</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="about_main_box">
                <div class="single_about" data-aos="fade-up" data-aos-duration="1200" data-aos-easing="ease-in-out">
                    <div class="single_img">
                        <img src="img/about_img_1.png" alt="">
                    </div>
                    <div class="single_content">
                        <p class="head_content">Dəyərlərimiz 1</p>
                        <p class="body_content">
                            Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti,
                            varı, malı, qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun.
                            Bu kişi həmişə övlad həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı
                            sürünü haylayıb uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu.
                            Çoban qoyunların içində dayandı. Bir az sonra duman çəkildi, hava işıqlaşdı, ancaq elə bir tufan qopdu ki,
                             çoban üzüqoylu yerə uzandı, qoyunlar bir yerə toplandılar. 
                        </p>
                    </div>
                </div>
                <div class="single_about" data-aos="fade-up" data-aos-duration="1200" data-aos-easing="ease-in-out">
                    <div class="single_img">
                        <img src="img/about_img_2.png" alt="">
                    </div>
                    <div class="single_content">
                        <p class="head_content">Dəyərlərimiz 1</p>
                        <p class="body_content">
                            Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti,
                            varı, malı, qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun.
                            Bu kişi həmişə övlad həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı
                            sürünü haylayıb uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu.
                            Çoban qoyunların içində dayandı. Bir az sonra duman çəkildi, hava işıqlaşdı, ancaq elə bir tufan qopdu ki,
                             çoban üzüqoylu yerə uzandı, qoyunlar bir yerə toplandılar. 
                        </p>
                    </div>
                </div>
                <div class="single_about" data-aos="fade-up" data-aos-duration="1200" data-aos-easing="ease-in-out">
                    <div class="single_img">
                        <img src="img/about_img_3.png" alt="">
                    </div>
                    <div class="single_content">
                        <p class="head_content">Dəyərlərimiz 1</p>
                        <p class="body_content">
                            Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti,
                            varı, malı, qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun.
                            Bu kişi həmişə övlad həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı
                            sürünü haylayıb uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu.
                            Çoban qoyunların içində dayandı. Bir az sonra duman çəkildi, hava işıqlaşdı, ancaq elə bir tufan qopdu ki,
                             çoban üzüqoylu yerə uzandı, qoyunlar bir yerə toplandılar. 
                        </p>
                    </div>
                </div>
            </div>
            <div class="swiper about_swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="slide_inner">
                            <div class="slide_img"><img src="img/slide_1.png" alt=""></div>
                            <div class="slide_content">
                                <div class="slide_heading">
                                    <p class="name_person">prof. Emily Doe</p>
                                    <p class="qual_person">Prorektor</p>
                                </div>
                                <p class="content_text">
                                    Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti, varı, malı, 
                                    qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun. Bu kişi həmişə övlad 
                                    həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı sürünü haylayıb 
                                    uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu. Çoban 
                                    qoyunların içində dayandı. Bir az sonra duman çəkildi, hava işıqlaşdı, ancaq elə bir tufan qopdu ki, çoban 
                                    üzüqoylu yerə uzandı, qoyunlar bir yerə toplandılar. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="slide_inner">
                            <div class="slide_img"><img src="img/slide_1.png" alt=""></div>
                            <div class="slide_content">
                                <div class="slide_heading">
                                    <p class="name_person">prof. Emily Doe</p>
                                    <p class="qual_person">Prorektor</p>
                                </div>
                                <p class="content_text">
                                    Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti, varı, malı, 
                                    qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun. Bu kişi həmişə övlad 
                                    həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı sürünü haylayıb 
                                    uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu. Çoban 
                                    qoyunların içində dayandı. Bir az sonra duman çəkildi, hava işıqlaşdı, ancaq elə bir tufan qopdu ki, çoban 
                                    üzüqoylu yerə uzandı, qoyunlar bir yerə toplandılar. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="slide_inner">
                            <div class="slide_img"><img src="img/slide_1.png" alt=""></div>
                            <div class="slide_content">
                                <div class="slide_heading">
                                    <p class="name_person">prof. Emily Doe</p>
                                    <p class="qual_person">Prorektor</p>
                                </div>
                                <p class="content_text">
                                    Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti, varı, malı, 
                                    qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun. Bu kişi həmişə övlad 
                                    həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı sürünü haylayıb 
                                    uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu. Çoban 
                                    qoyunların içində dayandı. Bir az sonra duman çəkildi, hava işıqlaşdı, ancaq elə bir tufan qopdu ki, çoban 
                                    üzüqoylu yerə uzandı, qoyunlar bir yerə toplandılar. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper_help_pagination_navigation">
                <button class="swiper-prev-button swiper_btn"><img src="img/swiper_button.svg" alt=""></button>
                <div class="swiper-pagination"></div>
                <button class="swiper-next-button swiper_btn"><img src="img/swiper_button.svg" alt=""></button>
            </div>
        </div>
    </div>

    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>