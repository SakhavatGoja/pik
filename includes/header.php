<header>
    <div class="container">
        <div class="row">
            <div class="header_main_container">
                <a href="index.php" class="header-logo"><img src="img/header-logo.svg" alt=""></a>
                <ul>
                    <li><a href="about.php">Haqqımızda</a></li>
                    <li><a href="#">Fakültələr</a></li>
                    <li><a href="fag.php">FAQ</a></li>
                    <li><a href="#">Əlaqə</a></li>
                    <div class="h-lang">
                        <a href="javascript:void(0)" class="h-lang-link dropdownButton">
                            az
                            <img src="img/down.svg" alt="">
                        </a>
                        <ul class="h-lang-list dropdownMenu" >
                            <li><a class="h-lang-item" href="#">ru</a></li>
                            <li><a class="h-lang-item" href="#">en</a></li>
                        </ul>
                    </div>
                </ul>
            </div>
        </div>
    </div>
</header>