<?php
    include("includes/head.php");
?>

<section class="index">
    <?php
        include("includes/header.php");
    ?>

    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>