var swiper = new Swiper(".about_swiper", {
  slidesPerGroup: 1,
  slidesPerView: 1,
  grabCursor: true,
  effect: "creative",
  autoplay:{
    pauseOnMouseEnter: false,
    disableOnInteraction: false
  },
  speed: 1500,
  creativeEffect: {
    prev: {
      translate: ["-120%", 0, -450],
    },
    next: {
      translate: ["120%", 0, -450],
    },
  },
  pagination: {
    el: '.swiper_help_pagination_navigation .swiper-pagination',
    type: 'bullets',
    clickable: true
  },
  navigation: {
    nextEl: '.swiper_help_pagination_navigation .swiper-next-button',
    prevEl: '.swiper_help_pagination_navigation .swiper-prev-button',
  }
});



