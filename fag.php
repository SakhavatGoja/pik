<?php
    include("includes/head.php");
?>

<section class="fag">
    <?php
        include("includes/header.php");
    ?>
    <div class="same_landing fag_landing">
        <div class="container">
            <div class="row">
                <p>Ən çox soruşulan suallar</p>
            </div>
        </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="fag_ul_box">
          <ul id="accordion" class="accordion">
            <li>
              <div class="link">
                  <p class="link_accordion">Ən çox verilən suallar 1</p>
                  <button></button>
              </div>
              <div class="submenu">
                  <p class="submenu_inner">
                  Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti, varı, malı, qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun. Bu kişi həmişə övlad həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı sürünü haylayıb uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu. Çoban qoyunların içində dayandı.
                  </p>
              </div>
            </li>
            <li>
              <div class="link">
                  <p class="link_accordion">Ən çox verilən suallar 1</p>
                  <button></button>
              </div>
              <div class="submenu">
                  <p class="submenu_inner">
                  Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti, varı, malı, qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun. Bu kişi həmişə övlad həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı sürünü haylayıb uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu. Çoban qoyunların içində dayandı. 
                  </p>
              </div>
            </li>
            <li>
              <div class="link">
                  <p class="link_accordion">Ən çox verilən suallar 1</p>
                  <button></button>
              </div>
              <div class="submenu">
                  <p class="submenu_inner">
                  Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti, varı, malı, qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun. Bu kişi həmişə övlad həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı sürünü haylayıb uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu. Çoban qoyunların içində dayandı.
                  </p>
              </div>
            </li>
            <li>
              <div class="link">
                  <p class="link_accordion">Ən çox verilən suallar 1</p>
                  <button></button>
              </div>
              <div class="submenu">
                  <p class="submenu_inner">
                  Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti, varı, malı, qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun. Bu kişi həmişə övlad həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı sürünü haylayıb uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu. Çoban qoyunların içində dayandı. 
                  </p>
              </div>
            </li>
            <li>
              <div class="link">
                  <p class="link_accordion">Ən çox verilən suallar 1</p>
                  <button></button>
              </div>
              <div class="submenu">
                  <p class="submenu_inner">
                  Biri var idi, biri yox idi, keçmiş zamanlarda Nəbi adlı varlı bir kişi vardı. Bu kişinin dövləti, varı, malı, qoyunu başından aşırdı, ancaq övladı yox idi ki, öləndən sonra dövlətinə sahib dursun. Bu kişi həmişə övlad həsrətilə yaşayırdı. Onun ağıllı bir çobanı var idi. Günlərin bir günü Nəbi kişinin çobanı sürünü haylayıb uca bir dağın ətəyinə gətirdi. Birdən duman, çiskin hər tərəfi bürüdü, ətraf gecə kimi qaranlıq oldu. Çoban qoyunların içində dayandı. 
                  </p>
              </div>
            </li>
          </ul>

        </div>
      </div>
    </div>

    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>